# Open Mobility Indicators API

Serve indicator, indicator data and catalog. omi-api is part of Open Mobility Indicators (see [project architecture](https://gitlab.com/open-mobility-indicators/website/-/wikis/4_Mainteneur/devops#quelle-est-larchitecture-de-la-plateforme-))

## Install

Use poetry:

```bash
poetry install
```

## Configure

```bash
cp .env.example .env
```

Adapt `.env` file content to your needs. Don't commit this file!

## Run

```bash
dotenv run poetry run uvicorn --factory omi_api:create_app --reload
```

## Tests

```bash
poetry run pytest
```

## Deploy

To deploy a new version:

- update version in `pyproject.toml` and `CHANGELOG` file
- `poetry update && poetry install`
- `git commit -a -m "Release"`
- `git tag $VERSION`
- `git push && git push --tags`
