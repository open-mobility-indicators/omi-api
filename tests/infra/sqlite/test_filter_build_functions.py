from omi_api.domain.repositories.indicator_data_repository import (
    IndicatorDataFilterDef,
    IndicatorDataSortDef,
)
from omi_api.infra.sqlite.indicator_data_repo import (
    build_where_and_parameter_values_from_filter_def,
)
from omi_api.infra.sqlite.utils import build_order_string_from_sort_def


def test_empty_filter():
    filter_def = IndicatorDataFilterDef(parameter_profile=None, slug=None)
    assert build_where_and_parameter_values_from_filter_def(filter_def) == ("", [])


def test_parameter_profile_filter():
    parameter_profile = "indre"
    filter_def = IndicatorDataFilterDef(parameter_profile=parameter_profile, slug=None)
    assert build_where_and_parameter_values_from_filter_def(filter_def) == (
        " WHERE parameter_profile = ?",
        [parameter_profile],
    )


def test_slug_filter():
    slug = "cycles"
    filter_def = IndicatorDataFilterDef(parameter_profile=None, slug=slug)
    assert build_where_and_parameter_values_from_filter_def(filter_def) == (
        " WHERE slug = ?",
        [slug],
    )


def test_combined_filter():
    parameter_profile = "indre"
    slug = "cycles"
    filter_def = IndicatorDataFilterDef(parameter_profile=parameter_profile, slug=slug)
    assert build_where_and_parameter_values_from_filter_def(filter_def) == (
        " WHERE parameter_profile = ? AND slug = ?",
        [parameter_profile, slug],
    )


def test_empty_sort_criteria():
    sort_by = []
    assert build_order_string_from_sort_def(sort_by) == ""


def test_one_sort_criteria():
    sort_by = [IndicatorDataSortDef(property_name="created_at", order="desc")]
    assert build_order_string_from_sort_def(sort_by) == " ORDER BY created_at DESC"


def test_multi_sort_criteria():
    sort_by = [
        IndicatorDataSortDef(property_name="created_at", order="desc"),
        IndicatorDataSortDef(property_name="slug", order="asc"),
    ]
    assert (
        build_order_string_from_sort_def(sort_by)
        == " ORDER BY created_at DESC, slug ASC"
    )
