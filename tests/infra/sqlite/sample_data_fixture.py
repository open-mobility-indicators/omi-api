import datetime

from omi_api.domain import IndicatorData, ParameterProfile, IndicatorSource

# This sample indicator list is used in several unit tests
# Please don't update data without re-running unit tests afterwards
sample_indicator_data_list = [
    IndicatorData(
        id="AAA",
        created_at=datetime.datetime.fromisoformat("2021-08-17T09:53:45+00:00"),
        parameter_profile=ParameterProfile(
            name="indre", values={"location_string": "Chabris, France"}
        ),
        pipeline_id="123456",
        slug="cycles",
        source=IndicatorSource(
            branch="master",
            commit="aaaa",
            project_url="https://www.example.com",
        ),
    ),
    IndicatorData(
        id="BBB",
        created_at=datetime.datetime.fromisoformat("2024-08-17T09:53:45+00:00"),
        parameter_profile=ParameterProfile(
            name="indre", values={"location_string": "Chabris, France"}
        ),
        pipeline_id="234567",
        slug="jupyter-notebook",
        source=IndicatorSource(
            branch="main",
            commit="bbbb",
            project_url="https://www.example2.com",
        ),
    ),
    IndicatorData(
        id="CCC",
        created_at=datetime.datetime.fromisoformat("2022-08-17T09:53:45+00:00"),
        parameter_profile=ParameterProfile(
            name="alpes-de-haute-provence",
            values={"location_string": "Dignes-les-Bains, France"},
        ),
        pipeline_id="345678",
        slug="cycles",
        source=IndicatorSource(
            branch="master",
            commit="cccc",
            project_url="https://www.example3.com",
        ),
    ),
    IndicatorData(
        id="DDD",
        created_at=datetime.datetime.fromisoformat("2023-08-17T09:53:45+00:00"),
        parameter_profile=ParameterProfile(
            name="alpes-de-haute-provence",
            values={"location_string": "Dignes-les-Bains, France"},
        ),
        pipeline_id="456789",
        slug="jupyter-notebook",
        source=IndicatorSource(
            branch="dev",
            commit="dddd",
            project_url="https://www.example4.com",
        ),
    ),
]
