import abc
from enum import Enum
from typing import Iterable, Iterator, Optional

from omi_api.domain.entities import IndicatorData, IndicatorDataId, IndicatorSlug
from pydantic import BaseModel

from .common import PaginationDef, SortOrderEnum

__all__ = [
    "IndicatorDataFilterDef",
    "IndicatorDataRepository",
    "IndicatorDataSortDef",
    "IndicatorDataSortProperty",
]


class IndicatorDataFilterDef(BaseModel):
    parameter_profile: Optional[str]
    slug: Optional[str]


class IndicatorDataSortProperty(str, Enum):
    created_at = "created_at"
    parameter_profile = "parameter_profile"
    slug = "slug"


class IndicatorDataSortDef(BaseModel):
    property_name: IndicatorDataSortProperty
    order: SortOrderEnum


class IndicatorDataRepository(abc.ABC):
    def get(self, indicator_id: IndicatorDataId) -> Optional[IndicatorData]:
        pass

    def find(
        self,
        *,
        filter_by: IndicatorDataFilterDef,
        sort_by: Optional[list[IndicatorDataSortDef]] = None,
        paginate_by: Optional[PaginationDef] = None,
    ) -> Iterator[IndicatorData]:
        pass

    def count(self, *, filter_by: IndicatorDataFilterDef) -> int:
        pass

    def reload_from_indicator_data(
        self, indicator_data_iter: Iterable[IndicatorData]
    ) -> None:
        pass

    def iter_slug(self) -> Iterator[IndicatorSlug]:
        pass


class CommonFileSystemIndicatorDataRepository(abc.ABC):
    def delete(self, indicator_data_id: IndicatorDataId):
        pass

    def compute_size(self, indicator_data_id: IndicatorDataId):
        pass


class FileSystemIndicatorDataRepository(CommonFileSystemIndicatorDataRepository):
    def iter_all(self) -> Iterator[IndicatorData]:
        pass


class FileSystemMbtilesRepository(CommonFileSystemIndicatorDataRepository):
    def reload_server(self) -> None:
        pass
