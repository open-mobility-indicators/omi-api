from dataclasses import dataclass
from typing import Optional


@dataclass
class Stat:
    mbtiles: Optional[int]
    output_files: Optional[int]
    total: Optional[int]
