from dataclasses import dataclass
from typing import Iterator, Optional, Sequence

from omi_api.domain.entities import IndicatorData, IndicatorDataId, Stat
from omi_api.domain.entities.indicator import IndicatorSlug
from omi_api.domain.repositories.common import Command, CommandReport, PaginationDef
from omi_api.domain.repositories.indicator_data_repository import (
    FileSystemIndicatorDataRepository,
    FileSystemMbtilesRepository,
    IndicatorDataFilterDef,
    IndicatorDataRepository,
    IndicatorDataSortDef,
)
from omi_api.domain.repositories.metadata_repository import CatalogMetadataRepository
from omi_api.infra.file_system import MbtilesReloadError
from omi_api.infra.file_system.errors import (
    IndicatorDataDirNotFound,
    MbtilesFileNotFound,
)

__all__ = ["IndicatorDataService"]


@dataclass
class IndicatorDataService:
    file_system_indicator_data_repo: FileSystemIndicatorDataRepository
    file_system_mbtiles_repo: FileSystemMbtilesRepository
    indicator_data_repo: IndicatorDataRepository
    catalog_metadata_repo: CatalogMetadataRepository

    def find_indicator_data(
        self,
        *,
        filter_by: IndicatorDataFilterDef,
        sort_by: Optional[list[IndicatorDataSortDef]] = None,
        paginate_by: Optional[PaginationDef] = None,
    ) -> tuple[int, Sequence[IndicatorData]]:
        total, indicator_data_items = (
            self.indicator_data_repo.count(filter_by=filter_by),
            list(
                self.indicator_data_repo.find(
                    filter_by=filter_by, sort_by=sort_by, paginate_by=paginate_by
                )
            ),
        )
        # Add catalog metadata info
        for indicator_data in indicator_data_items:
            indicator_data.pre_selected = self.catalog_metadata_repo.is_preselected(
                indicator_data.slug, indicator_data.id
            )
        return total, indicator_data_items

    def get_indicator_data_by_id(
        self,
        indicator_data_id: IndicatorDataId,
    ) -> Optional[IndicatorData]:
        indicator_data = self.indicator_data_repo.get(indicator_data_id)
        if indicator_data:
            # add catalog metadata info
            indicator_data.pre_selected = self.catalog_metadata_repo.is_preselected(
                indicator_data.slug, indicator_data.id
            )
        return indicator_data

    def delete_indicator_data_by_id(
        self, indicator_data_id: IndicatorDataId, *, reload: bool = True
    ) -> CommandReport:

        command_report = CommandReport()

        indicator_data_msg = f"delete indicator data dir {indicator_data_id!r}"
        try:
            self.file_system_indicator_data_repo.delete(indicator_data_id)
            command_report.add_cmd(Command.from_success(indicator_data_msg))
        except IndicatorDataDirNotFound:
            command_report.add_cmd(
                Command.from_error(indicator_data_msg, "indicator data dir not found")
            )
        if reload:
            command_report.add_cmd(self.reload())

        mbtiles_msg = f"delete mbtiles {indicator_data_id!r}"
        try:
            self.file_system_mbtiles_repo.delete(indicator_data_id)
            command_report.add_cmd(Command.from_success(mbtiles_msg))
        except MbtilesFileNotFound:
            command_report.add_cmd(
                Command.from_error(mbtiles_msg, "mbtiles file not found")
            )
        except Exception as ex:
            command_report.add_cmd(Command.from_exception(mbtiles_msg, ex))

        if reload:
            command_report.add_cmd(self.reload_mbtileserver())

        return command_report

    def reload_mbtileserver(self) -> Command:
        try:
            self.file_system_mbtiles_repo.reload_server()
            return Command.from_success("reload mbtileserver")
        except MbtilesReloadError as ex:
            return Command.from_exception("reload mbfileserver", ex)

    def compute_stat(self, indicator_data_id: IndicatorDataId) -> Stat:
        mbtiles = self.file_system_mbtiles_repo.compute_size(indicator_data_id)
        output_files = self.file_system_indicator_data_repo.compute_size(
            indicator_data_id
        )
        total = (
            None
            if mbtiles is None and output_files is None
            else mbtiles
            if output_files is None
            else output_files
            if mbtiles is None
            else mbtiles + output_files
        )
        return Stat(mbtiles=mbtiles, output_files=output_files, total=total)

    def iter_slug(self) -> Iterator[IndicatorSlug]:
        yield from self.indicator_data_repo.iter_slug()

    def reload(self) -> Command:
        try:
            indicator_data = self.file_system_indicator_data_repo.iter_all()
            self.indicator_data_repo.reload_from_indicator_data(indicator_data)
            return Command.from_success("reload indicator data")
        except Exception as ex:
            return Command.from_exception("reload indicator data", ex)
