import json
import sqlite3
from typing import Iterable, Iterator, Optional

from omi_api.domain import (
    IndicatorData,
    IndicatorDataFilterDef,
    IndicatorDataId,
    IndicatorDataRepository,
    IndicatorDataSortDef,
)
from omi_api.domain.entities.indicator import IndicatorSlug
from omi_api.domain.repositories.common import PaginationDef

from .model import SqliteIndicatorDataRow
from .utils import build_order_string_from_sort_def, dict_factory

__all__ = ["SQLiteIndicatorDataRepo"]

TABLE_NAME = "indicator_data"
CREATE_TABLE_QUERY = f"""
    CREATE TABLE {TABLE_NAME} (
        id TEXT PRIMARY KEY,
        created_at TEXT,
        parameter_profile TEXT,
        parameter_profile_values TEXT,
        pipeline_id TEXT,
        slug TEXT,
        source_branch TEXT,
        source_commit TEXT,
        source_project_url TEXT
    )
"""
INSERT_QUERY = f"""
    INSERT INTO {TABLE_NAME}
        (id, created_at, parameter_profile, parameter_profile_values,
         pipeline_id, slug, source_branch, source_commit, source_project_url)
        VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)
"""


def build_where_and_parameter_values_from_filter_def(
    filter_def: IndicatorDataFilterDef,
) -> tuple[str, list]:
    where_criteria = []
    parameter_values = []
    if filter_def.parameter_profile:
        where_criteria.append("parameter_profile = ?")
        parameter_values.append(filter_def.parameter_profile)
    if filter_def.slug:
        where_criteria.append("slug = ?")
        parameter_values.append(filter_def.slug)

    if not where_criteria:
        return ("", [])
    return (" WHERE " + " AND ".join(where_criteria), parameter_values)


class SQLiteIndicatorDataRepo(IndicatorDataRepository):
    """Repository loading IndicatorData entities from a SQLite database."""

    @classmethod
    def from_indicator_data(
        cls, indicator_data: Iterable[IndicatorData]
    ) -> "SQLiteIndicatorDataRepo":
        instance = cls()
        instance.reload_from_indicator_data(indicator_data)
        return instance

    def reload_from_indicator_data(
        self, indicator_data_iter: Iterable[IndicatorData]
    ) -> None:
        self._conn = sqlite3.connect(":memory:")
        self._conn.row_factory = dict_factory

        cur = self._conn.cursor()
        cur.execute(CREATE_TABLE_QUERY)
        self._conn.commit()

        for indicator_data in indicator_data_iter:
            cur.execute(
                INSERT_QUERY,
                (
                    indicator_data.id,
                    indicator_data.created_at,
                    indicator_data.parameter_profile.name,
                    json.dumps(indicator_data.parameter_profile.values),
                    indicator_data.pipeline_id,
                    indicator_data.slug,
                    indicator_data.source.branch,
                    indicator_data.source.commit,
                    indicator_data.source.project_url,
                ),
            )
        self._conn.commit()

    def get(self, indicator_id: IndicatorDataId) -> Optional[IndicatorData]:
        cur = self._conn.cursor()
        cur.execute(f"SELECT * FROM {TABLE_NAME} WHERE id=?", (indicator_id,))
        row = cur.fetchone()
        if row is None:
            return None
        return SqliteIndicatorDataRow.parse_obj(row).to_domain()

    def find(
        self,
        *,
        filter_by: IndicatorDataFilterDef,
        sort_by: Optional[list[IndicatorDataSortDef]] = None,
        paginate_by: Optional[PaginationDef] = None,
    ) -> Iterator[IndicatorData]:

        where_string, where_values = build_where_and_parameter_values_from_filter_def(
            filter_by
        )

        order_string = build_order_string_from_sort_def(sort_by)

        pagination_string, pagination_values = "", []
        if paginate_by:
            pagination_string = " LIMIT ? OFFSET ?"
            pagination_values = [paginate_by.limit, paginate_by.offset]

        query_values = where_values + pagination_values
        cur = self._conn.cursor()
        cur.execute(
            f"SELECT * FROM {TABLE_NAME}{where_string}{order_string}{pagination_string}",
            query_values,
        )

        for row in cur.fetchall():
            yield SqliteIndicatorDataRow.parse_obj(row).to_domain()

    def iter_slug(self) -> Iterator[IndicatorSlug]:
        """Iterates on slugs."""

        cur = self._conn.cursor()
        cur.execute(f"SELECT DISTINCT slug FROM {TABLE_NAME} ORDER BY 1")
        yield from (row["slug"] for row in cur.fetchall())

    def count(
        self,
        *,
        filter_by: IndicatorDataFilterDef,
    ) -> int:

        where_string, where_values = build_where_and_parameter_values_from_filter_def(
            filter_by
        )

        cur = self._conn.cursor()
        cur.execute(
            f"SELECT COUNT(*) AS total FROM {TABLE_NAME}{where_string}",
            where_values,
        )

        return cur.fetchone()["total"]
