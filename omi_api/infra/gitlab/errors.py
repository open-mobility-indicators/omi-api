from dataclasses import dataclass

from omi_api.domain.entities.indicator import IndicatorSlug
from pydantic.networks import AnyHttpUrl


@dataclass
class IndicatorMetadataError(Exception):
    indicator_slug: IndicatorSlug
    indicator_metadata_url: str


class IndicatorMetadataNotFound(IndicatorMetadataError):
    pass


class InvalidYaml(IndicatorMetadataError):
    pass


class InvalidIndicatorMetadata(IndicatorMetadataError):
    pass


@dataclass
class CatalogMetadataError(Exception):
    catalog_metadata_url: AnyHttpUrl


class CatalogMetadataNotFound(CatalogMetadataError):
    pass


class InvalidCatalogMetadata(CatalogMetadataError):
    pass


class InvalidCatalogMetadataFormat(CatalogMetadataError):
    pass


class GitLabProjectNotFound(Exception):
    pass


class BranchNotFound(Exception):
    pass
