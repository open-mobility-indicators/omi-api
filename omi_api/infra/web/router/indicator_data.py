from typing import Optional

from dependency_injector.wiring import Provide, inject
from fastapi import Depends, HTTPException, Query, Request
from omi_api.domain.entities.indicator_data import IndicatorData
from omi_api.domain.repositories.common import (
    PaginatedResults,
    PaginationDef,
    StatsMode,
)
from omi_api.domain.repositories.indicator_data_repository import (
    IndicatorDataFilterDef,
    IndicatorDataSortDef,
    IndicatorDataSortProperty,
)
from omi_api.domain.services.indicator_data_service import IndicatorDataService
from omi_api.infra.container import Container
from omi_api.infra.web.model import (
    IndicatorDataListWithStatsDTO,
    IndicatorDataWithStatsDTO,
    IndicatorMapper,
)
from omi_api.infra.web.query_utils import (
    build_sortdef_list_from_string,
    extract_admin_token,
)


@inject
async def find(
    parameter_profile: Optional[str] = None,
    slug: Optional[str] = None,
    sort: Optional[str] = Query(
        "created_at:desc",
        title="sort criteria",
        description=(
            "Sort criteria formatted as is: {property}:{order}."
            f" {{property}} must be one of those: {', '.join(item.value for item in IndicatorDataSortProperty)}."
            " {order} must be asc or desc."
            " To use multiple sort queries, separate them with ','."
        ),
    ),
    stats_mode: StatsMode = None,
    page: int = Query(1, ge=1),
    size: int = Query(50, ge=1, le=100),
    indicator_data_service: IndicatorDataService = Depends(
        Provide[Container.indicator_data_service]
    ),
    indicator_mapper: IndicatorMapper = Depends(Provide[Container.indicator_mapper]),
) -> IndicatorDataListWithStatsDTO:
    """Find indicators by slug or parameter profile."""
    try:
        sort_by = build_sortdef_list_from_string(
            sort, IndicatorDataSortProperty, IndicatorDataSortDef
        )
    except SyntaxError as err:
        raise HTTPException(status_code=400, detail=err.msg)

    filter_def = IndicatorDataFilterDef(parameter_profile=parameter_profile, slug=slug)
    paginate_def = PaginationDef(page=page, size=size)

    total, indicator_data_items = indicator_data_service.find_indicator_data(
        filter_by=filter_def, sort_by=sort_by, paginate_by=paginate_def
    )
    paginated_results: PaginatedResults[IndicatorData] = PaginatedResults(
        items=indicator_data_items,
        page=paginate_def.page,
        size=paginate_def.size,
        total=total,
    )

    return indicator_mapper.indicator_data_list_to_with_stats_dto(
        paginated_results, stats_mode=stats_mode
    )


@inject
async def get_indicator_data_by_id(
    indicator_data_id: str,
    stats_mode: StatsMode = None,
    indicator_data_service: IndicatorDataService = Depends(
        Provide[Container.indicator_data_service]
    ),
    indicator_mapper: IndicatorMapper = Depends(Provide[Container.indicator_mapper]),
) -> IndicatorDataWithStatsDTO:
    """Get indicator data by ID."""
    indicator_data = indicator_data_service.get_indicator_data_by_id(indicator_data_id)
    if indicator_data is None:
        raise HTTPException(status_code=404, detail="Indicator data not found")
    return indicator_mapper.indicator_data_to_with_stats_dto(
        indicator_data, stats_mode=stats_mode
    )


@inject
async def delete_indicator_data_by_id(
    request: Request,
    indicator_data_id: str,
    admin_token: str = None,  # Make it appear in swagger interface for practical ue
    reference_admin_token: str = Depends(Provide[Container.config.admin_token]),
    indicator_data_service: IndicatorDataService = Depends(
        Provide[Container.indicator_data_service]
    ),
    indicator_mapper: IndicatorMapper = Depends(Provide[Container.indicator_mapper]),
):
    """Delete indicator data by ID."""
    user_token = await extract_admin_token(request)
    if user_token is None or user_token != reference_admin_token:
        raise HTTPException(status_code=403, detail="Wrong or missing token")

    report = indicator_data_service.delete_indicator_data_by_id(indicator_data_id)
    return indicator_mapper.command_report_to_dto(report)
