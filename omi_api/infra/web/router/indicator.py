from typing import Optional

from dependency_injector.wiring import Provide, inject
from fastapi import Depends, HTTPException, Query, Request
from omi_api.domain.entities.indicator import Indicator
from omi_api.domain.repositories.common import (
    PaginatedResults,
    PaginationDef,
    StatsMode,
)
from omi_api.domain.repositories.indicator_repository import (
    IndicatorDataMode,
    IndicatorSortDef,
    IndicatorSortProperty,
)
from omi_api.domain.services.indicator_service import IndicatorService
from omi_api.infra.container import Container
from omi_api.infra.web.model import (
    IndicatorListWithStatsDTO,
    IndicatorMapper,
    IndicatorWithStatsDTO,
)
from omi_api.infra.web.query_utils import (
    build_indicator_parameter_profiles_from_string,
    build_sortdef_list_from_string,
    extract_admin_token,
)


@inject
async def list(
    pre_selected: bool = False,
    indicator_data_mode: IndicatorDataMode = None,
    stats_mode: StatsMode = None,
    sort: Optional[str] = Query(
        "slug:asc",
        title="sort criteria",
        description=(
            "Sort criteria formatted as is: {property}:{order}."
            f" {{property}} must be one of those: {', '.join(item.value for item in IndicatorSortProperty)}."
            " {order} must be asc or desc."
            " To use multiple sort queries, separate them with ','."
        ),
    ),
    page: int = Query(1, ge=1),
    size: int = Query(10, ge=1, le=50),
    indicator_service: IndicatorService = Depends(Provide[Container.indicator_service]),
    indicator_mapper: IndicatorMapper = Depends(Provide[Container.indicator_mapper]),
) -> IndicatorListWithStatsDTO:
    """List indicators."""
    try:
        sort_by = build_sortdef_list_from_string(
            sort, IndicatorSortProperty, IndicatorSortDef
        )
    except SyntaxError as err:
        raise HTTPException(status_code=400, detail=err.msg)

    paginate_def = PaginationDef(page=page, size=size)
    total, indicator_items = indicator_service.list_indicators(
        pre_selected=pre_selected,
        sort_by=sort_by,
        paginate_by=paginate_def,
    )
    paginated_results: PaginatedResults[Indicator] = PaginatedResults(
        items=indicator_items,
        page=paginate_def.page,
        size=paginate_def.size,
        total=total,
    )

    return indicator_mapper.indicator_list_to_with_stats_dto(
        paginated_results,
        indicator_data_mode=indicator_data_mode,
        stats_mode=stats_mode,
    )


@inject
async def get_indicator_by_slug(
    indicator_slug: str,
    indicator_data_mode: IndicatorDataMode = None,
    stats_mode: StatsMode = None,
    indicator_service: IndicatorService = Depends(Provide[Container.indicator_service]),
    indicator_mapper: IndicatorMapper = Depends(Provide[Container.indicator_mapper]),
) -> IndicatorWithStatsDTO:
    """Get indicator by slug."""
    indicator = indicator_service.get_indicator_by_slug(indicator_slug)
    if indicator is None:
        raise HTTPException(status_code=404, detail="Indicator not found")
    return indicator_mapper.indicator_to_with_stats_dto(
        indicator, indicator_data_mode=indicator_data_mode, stats_mode=stats_mode
    )


@inject
async def delete_indicator_by_slug(
    request: Request,
    indicator_slug: str,
    admin_token: str = None,  # Make it appear in swagger interface for practical use
    reference_admin_token: str = Depends(Provide[Container.config.admin_token]),
    indicator_service: IndicatorService = Depends(Provide[Container.indicator_service]),
    indicator_mapper: IndicatorMapper = Depends(Provide[Container.indicator_mapper]),
):
    """Delete indicator by slug."""
    user_token = await extract_admin_token(request)
    if user_token is None or user_token != reference_admin_token:
        raise HTTPException(status_code=403, detail="Wrong or missing token")

    report = indicator_service.delete_indicator_by_slug(indicator_slug)
    return indicator_mapper.command_report_to_dto(report)


@inject
async def run_indicator_pipelines(
    request: Request,
    indicator_slug: str,
    branch_name: Optional[str] = Query(
        None,
        title="project branch to use",
        description="Let it blank to use default branch.",
    ),
    parameter_profiles_names: str = Query(
        "*",
        title="parameter profile name list",
        description=(
            "Use '*' to run pipeline on all defined parameter profiles or"
            " specify one or more parameter profile names separated by ','."
        ),
    ),
    admin_token: str = None,  # Make it appear in swagger interface for practical use
    reference_admin_token: str = Depends(Provide[Container.config.admin_token]),
    indicator_service: IndicatorService = Depends(Provide[Container.indicator_service]),
    indicator_mapper: IndicatorMapper = Depends(Provide[Container.indicator_mapper]),
):
    """Run one or more pipelines on the indicator project."""
    user_token = await extract_admin_token(request)
    if user_token is None or user_token != reference_admin_token:
        raise HTTPException(status_code=403, detail="Wrong or missing token")

    try:
        parameter_profiles = build_indicator_parameter_profiles_from_string(
            parameter_profiles_names
        )
    except SyntaxError as err:
        raise HTTPException(status_code=400, detail=err.msg)

    report = indicator_service.run_indicator_pipelines(
        indicator_slug, parameter_profiles=parameter_profiles, branch_name=branch_name
    )
    return indicator_mapper.command_report_to_dto(report)
