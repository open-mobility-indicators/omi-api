import re
from typing import Optional

from fastapi import Request
from omi_api.domain.repositories.indicator_repository import IndicatorParameterProfiles

SORT_CRITERIA_RE = re.compile(r"^(\w+):(asc|desc)$")


def build_sortdef_list_from_string(
    sort_criteria: str, sort_fields_class, sort_def_class
) -> Optional[list]:
    text_to_parse = re.sub(r"\s", "", sort_criteria)
    if not text_to_parse:
        return []

    sort_def_list = []
    for criteria_str in text_to_parse.split(","):
        m = SORT_CRITERIA_RE.match(criteria_str)
        if not m:
            raise SyntaxError(f"malformed sort criteria: {sort_criteria}")
        prop = m.group(1)
        order = m.group(2)
        if not hasattr(sort_fields_class, prop):
            raise SyntaxError(f"unknown property: {prop}")
        sort_def_list.append(sort_def_class(property_name=prop, order=order))
    return sort_def_list


def build_indicator_parameter_profiles_from_string(
    parameter_profiles_str: str,
) -> IndicatorParameterProfiles:
    parameter_profiles_str = parameter_profiles_str.strip()
    if parameter_profiles_str == "*":
        return "all"
    profile_list = list(
        filter(lambda elt: elt != "", map(str.strip, parameter_profiles_str.split(",")))
    )
    if not profile_list:
        raise SyntaxError("No parameter profile given")
    return profile_list


async def extract_admin_token(request: Request) -> Optional[str]:
    """Extract and return admin token if found.

    Token is searched in query parameter ("admin_token")
    If not found, it's searched in form parameter ("admin_token")
    Otherwise it's looked for in HTTP request headers ("X-ADMIN-TOKEN")
    """
    if "admin_token" in request.query_params:
        return request.query_params["admin_token"]
    form_data = await request.form()
    if "admin_token" in form_data:
        return form_data["admin_token"]
    return request.headers.get("X-ADMIN-TOKEN")
