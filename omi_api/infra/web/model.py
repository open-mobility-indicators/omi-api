from dataclasses import dataclass
from datetime import datetime
from typing import Any, Callable, Generic, Optional, Tuple, TypeVar, Union

from omi_api.domain import Indicator, IndicatorData, IndicatorDataId, Stat
from omi_api.domain.entities.indicator_data import IndicatorSource, ParameterProfile
from omi_api.domain.repositories.common import (
    Command,
    CommandReport,
    CommandStatus,
    PaginatedResults,
    StatsMode,
)
from omi_api.domain.repositories.indicator_data_repository import IndicatorDataFilterDef
from omi_api.domain.repositories.indicator_repository import IndicatorDataMode
from omi_api.domain.services.indicator_data_service import IndicatorDataService
from omi_api.domain.services.indicator_service import IndicatorService
from pydantic import BaseModel

__all__ = [
    "ParameterProfileDTO",
    "IndicatorSourceDTO",
    "IndicatorDataDTO",
    "IndicatorDTO",
    "IndicatorDataWithStatsDTO",
]


class ParameterProfileDTO(BaseModel):
    name: str
    values: dict[str, Any]


class IndicatorSourceDTO(BaseModel):
    branch: str
    commit: str
    project_url: str


class IndicatorDataDTO(BaseModel):
    id: str
    created_at: datetime
    parameter_profile: ParameterProfileDTO
    pipeline_id: str
    pre_selected: bool = False
    slug: str
    source: IndicatorSourceDTO


class StatDTO(BaseModel):
    mbtiles: Optional[int]
    output_files: Optional[int]
    total: Optional[int]

    @staticmethod
    def from_domain(stat: Stat):
        return StatDTO(
            mbtiles=stat.mbtiles, output_files=stat.output_files, total=stat.total
        )


class StatsDTO(BaseModel):
    disk_usage: dict[str, StatDTO]

    @staticmethod
    def from_stat_list(stat_list: list[Tuple[str, Stat]]):
        return (
            StatsDTO(disk_usage={k: StatDTO.from_domain(v) for k, v in stat_list})
            if stat_list
            else None
        )


class IndicatorDataWithStatsDTO(BaseModel):
    indicator_data: IndicatorDataDTO
    stats: Optional[StatsDTO]


T = TypeVar("T")


class PaginatedResultsDTO(BaseModel, Generic[T]):
    items: list[T]
    total: int
    page: int
    size: int


class IndicatorDataListWithStatsDTO(BaseModel):
    indicator_data: PaginatedResultsDTO[IndicatorDataDTO]
    stats: Optional[StatsDTO]


IndicatorDataField = Union[None, list[IndicatorDataId], list[IndicatorDataDTO]]


class IndicatorDTO(BaseModel):
    description: Optional[str]
    indicator_data: IndicatorDataField
    name: str
    popup_template: Optional[str]
    pre_selected: bool = False
    slug: str
    work_in_progress: bool = False


class IndicatorWithStatsDTO(BaseModel):
    indicator: IndicatorDTO
    stats: Optional[StatsDTO]


class IndicatorListWithStatsDTO(BaseModel):
    indicators: PaginatedResultsDTO[IndicatorDTO]
    stats: Optional[StatsDTO]


class CommandDTO(BaseModel):
    name: str
    status: CommandStatus
    message: Optional[str] = None

    @staticmethod
    def from_domain(cmd: Command):
        return CommandDTO(name=cmd.name, status=cmd.status, message=cmd.message)


class CommandReportDTO(BaseModel):
    report: list[CommandDTO]

    @staticmethod
    def from_domain(command_report: CommandReport):
        return CommandReportDTO(
            report=[CommandDTO.from_domain(unit) for unit in command_report.commands]
        )


@dataclass
class IndicatorMapper:
    indicator_data_service: IndicatorDataService
    indicator_service: IndicatorService

    def _build_indicator_data_field(
        self, indicator: Indicator, indicator_data_mode: IndicatorDataMode
    ) -> IndicatorDataField:
        if indicator_data_mode is None:
            return None

        _, indicator_data_list = self.indicator_data_service.find_indicator_data(
            filter_by=IndicatorDataFilterDef(slug=indicator.slug)
        )

        if indicator_data_mode == "ids":
            return [indicator_data.id for indicator_data in indicator_data_list]

        if indicator_data_mode == "full":
            return [
                self.indicator_data_to_dto(indicator_data)
                for indicator_data in indicator_data_list
            ]

        raise ValueError(
            f"Unexpected value for indicator_data_mode: {indicator_data_mode!r}"
        )

    def indicator_to_dto(
        self, indicator: Indicator, *, indicator_data_mode: IndicatorDataMode
    ) -> IndicatorDTO:
        indicator_data_field = self._build_indicator_data_field(
            indicator, indicator_data_mode
        )
        return IndicatorDTO(
            description=indicator.description,
            indicator_data=indicator_data_field,
            name=indicator.name,
            popup_template=indicator.popup_template,
            pre_selected=indicator.pre_selected,
            slug=indicator.slug,
            work_in_progress=indicator.work_in_progress,
        )

    def indicator_to_with_stats_dto(
        self,
        indicator: Indicator,
        *,
        indicator_data_mode: IndicatorDataMode,
        stats_mode: StatsMode,
    ) -> IndicatorWithStatsDTO:
        indicator_dto = self.indicator_to_dto(
            indicator, indicator_data_mode=indicator_data_mode
        )
        stats_dto = self.compute_indicator_stats_dto(stats_mode, [indicator.slug])
        return IndicatorWithStatsDTO(indicator=indicator_dto, stats=stats_dto)

    def indicator_data_to_dto(self, indicator_data: IndicatorData) -> IndicatorDataDTO:
        return IndicatorDataDTO(
            id=indicator_data.id,
            created_at=indicator_data.created_at,
            parameter_profile=self.parameter_profile_to_dto(
                indicator_data.parameter_profile
            ),
            pipeline_id=indicator_data.pipeline_id,
            pre_selected=indicator_data.pre_selected,
            slug=indicator_data.slug,
            source=self.source_to_dto(indicator_data.source),
        )

    def compute_stats_dto(
        self, stats_mode: StatsMode, id_list: list[str], do_stat: Callable
    ):
        if stats_mode is None:
            return None
        return StatsDTO.from_stat_list(
            [(id, StatDTO.from_domain(do_stat(id))) for id in id_list]
        )

    def compute_indicator_data_stats_dto(
        self, stats_mode: StatsMode, id_list: list[str]
    ):
        do_stat = self.indicator_data_service.compute_stat
        return self.compute_stats_dto(stats_mode, id_list, do_stat)

    def compute_indicator_stats_dto(self, stats_mode: StatsMode, id_list: list[str]):
        do_stat = self.indicator_service.compute_stat
        return self.compute_stats_dto(stats_mode, id_list, do_stat)

    def indicator_data_to_with_stats_dto(
        self, indicator_data: IndicatorData, *, stats_mode: StatsMode
    ) -> IndicatorDataWithStatsDTO:
        indicator_data_dto = self.indicator_data_to_dto(indicator_data)
        stats_dto = self.compute_indicator_data_stats_dto(
            stats_mode, [indicator_data.id]
        )
        return IndicatorDataWithStatsDTO(
            indicator_data=indicator_data_dto, stats=stats_dto
        )

    def indicator_data_list_to_with_stats_dto(
        self,
        paginated_results: PaginatedResults[IndicatorData],
        stats_mode: StatsMode,
    ) -> IndicatorDataListWithStatsDTO:
        paging_dto: PaginatedResultsDTO[IndicatorData] = PaginatedResultsDTO(
            items=list(map(self.indicator_data_to_dto, paginated_results.items)),
            total=paginated_results.total,
            page=paginated_results.page,
            size=paginated_results.size,
        )
        stats_dto = self.compute_indicator_data_stats_dto(
            stats_mode,
            [item.id for item in paginated_results.items],
        )
        return IndicatorDataListWithStatsDTO(indicator_data=paging_dto, stats=stats_dto)

    def indicator_list_to_with_stats_dto(
        self,
        paginated_results: PaginatedResults[Indicator],
        *,
        indicator_data_mode: IndicatorDataMode,
        stats_mode: StatsMode,
    ) -> IndicatorListWithStatsDTO:
        paging_dto: PaginatedResultsDTO[IndicatorData] = PaginatedResultsDTO(
            items=list(
                map(
                    lambda elt: self.indicator_to_dto(
                        elt, indicator_data_mode=indicator_data_mode
                    ),
                    paginated_results.items,
                )
            ),
            total=paginated_results.total,
            page=paginated_results.page,
            size=paginated_results.size,
        )
        stats_dto = self.compute_indicator_stats_dto(
            stats_mode, [item.slug for item in paginated_results.items]
        )
        return IndicatorListWithStatsDTO(indicators=paging_dto, stats=stats_dto)

    def parameter_profile_to_dto(
        self, parameter_profile: ParameterProfile
    ) -> ParameterProfileDTO:

        return ParameterProfileDTO(
            name=parameter_profile.name,
            values=parameter_profile.values,
        )

    def source_to_dto(self, indicator_source: IndicatorSource) -> IndicatorSourceDTO:
        return IndicatorSourceDTO(
            branch=indicator_source.branch,
            commit=indicator_source.commit,
            project_url=indicator_source.project_url,
        )

    def command_report_to_dto(self, reload_report: CommandReport) -> CommandReportDTO:
        return CommandReportDTO.from_domain(reload_report)
