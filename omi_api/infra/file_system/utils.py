from pathlib import Path
from typing import Iterator, Optional


def iter_sub_directories(dir_path: Path) -> Iterator[Path]:
    for child in dir_path.iterdir():
        if child.is_dir():
            yield child


def get_file_size(f: Path) -> Optional[int]:
    if not f.is_file():
        return None
    return f.stat().st_size
